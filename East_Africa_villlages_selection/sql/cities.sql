drop table if exists worldcities_ea;
create table worldcities_ea as (
    select worldcities.*,
    st_buffer(worldcities.geom::geography, 50000)::geometry as buffer50,
    st_buffer(worldcities.geom::geography, 100000)::geometry as buffer100
    from worldcities where iso3 in ('COM', 'SDN', 'SSD', 'MYT', 'COD', 'ZWE', 'MDG', 'RWA', 'ERI', 'TZA', 'ZMB', 'ZAF', 'KEN', 'SOM', 'DJI', 'UGA', 'MWI', 'SWZ', 'BDI', 'MOZ', 'LSO', 'ETH') and population >= 50000
);
