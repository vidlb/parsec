drop table if exists osm_places_ea_d_wdpa_selection;
create table if not exists osm_places_ea_d_wdpa_selection as (
    select a.fid, b.wdpa_pid, b.name, b.distance/1000 as dist_km
    from osm_places_ea a
    join lateral (
        select wdpa_pid, name, a.geog <-> coalesce(geog, geog_point) as distance
        from wdpa_ea_selection
        order by distance limit 1
    ) as b on true
);

drop table if exists osm_places_ea_d_wdpa_other;
create table if not exists osm_places_ea_d_wdpa_other as (
    select a.fid, b.wdpa_pid, b.name, b.distance/1000 as dist_km
    from osm_places_ea a
    join lateral (
        select wdpa_pid, name,
        a.geog <-> coalesce(geog, geog_point) as distance
        from wdpa_ea
        where wdpa_pid not in ('555624671', '555697518', '555697527', '555698167', '555698171', '555705345_A', '20295', '342517', '342654', '365024', '555566988', '303695', '20273', '303702', '351704', '351706')
        and iucn_cat in ('Ia', 'Ib', 'II', 'III', 'IV')
        and rep_area > 1
        order by distance limit 1
    ) as b on true
);

drop table if exists osm_places_ea_d_coastlines;
create table if not exists osm_places_ea_d_coastlines as (
    select a.fid, b.ogc_fid, b.distance/1000 as dist_km from osm_places_ea a
    join lateral (
        select ogc_fid, a.geog <-> geog as distance
        from osm_coastlines order by distance limit 1
    ) as b on true
);


/**
drop table if exists osm_places_ea_d_fua;
create table if not exists osm_places_ea_d_fua as (
    select a.fid, b.efua_id, b.efua_name, b.distance/1000 as dist_km
    from osm_places_ea a
    join lateral (
        select efua_id, efua_name, a.geog <-> geog as distance
        from ghs_ucdb_fua order by distance limit 1
    ) as b on true
);
**/
drop table if exists osm_places_d_worldcities;
create table if not exists osm_places_d_worldcities as (
    select a.fid, b.id, b.city, b.iso3, b.population, b.distance/1000 as dist_km
    from osm_places_ea a
    join lateral (
        select id, city, iso3, population, geog <-> a.geog as distance
        from worldcities_ea
        where population >= 20000
        order by distance limit 1
    ) as b on true
);
    
drop table if exists osm_places_ea_preselection;
create table osm_places_ea_preselection as (
    select o.fid, o.id, o.lat, o.lon, o.country, o.place, o.name, o.population, o.geometry,
    a.dist_km as d_wdpa_selection,
    a.wdpa_pid, a.name as wdpa_name,
    b.dist_km as d_wdpa_other,
    c.dist_km as d_coastlines,
    w.city as n_city_name, w.population as n_city_pop, w.dist_km as n_city_dist
    --d.dist_km as d_fua
    from osm_places_ea o
    inner join osm_places_ea_d_wdpa_selection a on o.fid=a.fid
    inner join osm_places_ea_d_wdpa_other b on o.fid=b.fid
    inner join osm_places_ea_d_coastlines c on o.fid=c.fid
    inner join osm_places_d_worldcities w on o.fid=w.fid
    --inner join osm_places_ea_d_fua d on o.fid=d.fid
    where a.dist_km  < 20
    and b.dist_km > 0
    and c.dist_km > 5
);

/**
alter table osm_places_ea_preselection add column city20k_50km integer;
alter table osm_places_ea_preselection add column city20k_100km integer;
update osm_places_ea_preselection set city20k_50km=0;
update osm_places_ea_preselection set city20k_100km=0;

update osm_places_ea_preselection set city20k_50km=1 from worldcities_ea where st_intersects(geometry, buffer50);
update osm_places_ea_preselection set city20k_100km=1 from worldcities_ea where st_intersects(geometry, buffer100);
**/
