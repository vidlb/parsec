alter table osm_places_ea_full add column geog geography(Point);
update osm_places_ea_full set geog = geometry::geography;
create index wdpa_ea_selection_geom_idx on wdpa_ea_selection using gist(shape);
create index wdpa_ea_selection_geog_idx on wdpa_ea_selection using gist(geog);
create index osm_places_ea_full_geog_idx on osm_places_ea_full using gist(geog);
create index osm_places_ea_full_geom_idx on osm_places_ea_full using gist(geometry);
drop index if exists wdpa_ea_geog_idx;
create index wdpa_ea_geog_idx on wdpa_ea using gist(geog);
drop index if exists wdpa_ea_geog_point_idx;
create index wdpa_ea_geog_point_idx on wdpa_ea using gist(geog_point);
drop index if exists worldcities_ea_geog_idx;
create index worldcities_ea_geog_idx on worldcities_ea using gist(geog);

drop table if exists possible_mirrors;
create table possible_mirrors as (
    select row_number() over() as fid, o.id as osm_id, o.country, o.place, o.name, o.population, 
                                    o.geometry as geom, o.geometry::geography as geog
    from osm_places_ea_full o
    join 
        (select st_union(st_buffer(geog, 200000)::geometry) as geom from wdpa_ea_selection)
    as w on st_intersects(o.geometry, w.geom)
    where o.place != 'city' and coalesce(o.population, 0) < 20000
);

create index possible_mirrors_geom_idx on possible_mirrors using gist(geom);
create index possible_mirrors_geog_idx on possible_mirrors using gist(geog);

drop table if exists tmp_mirrors;
create table tmp_mirrors as (
    select p.fid,
    p.osm_id , p.country, p.place, p.name, p.population, 
    s.dist / 1000 as d_wdpa_ea_selection,
    c.dist / 1000 as d_coastline,
    a.dist / 1000 as d_wdpa_any,
    w.dist / 1000 as d_cities_20k,
    --mg.dist / 1000 as d_mangroves,
    --cr.dist / 1000 as d_corals,
    p.geom, p.geog

    from possible_mirrors p
    join lateral (
        select geog <->  p.geog as dist
        from wdpa_ea_selection
        order by dist limit 1
    ) as s on true

    join lateral (
        select geog <->  p.geog as dist
        from osm_coastlines
        order by dist limit 1
    ) as c on true
    
    join lateral (
        select coalesce(geog, geog_point) <-> p.geog as dist
        from wdpa_ea
        where wdpa_pid not in ('555624671', '555697518', '555697527', '555698167', '555698171', '555705345_A', '20295', '342517', '342654', '365024', '555566988', '303695', '20273', '303702', '351704', '351706')
        and iucn_cat in ('Ia', 'Ib', 'II', 'III', 'IV')
        and rep_area > 1
        order by dist limit 1
    ) as a on true

    join lateral (
        select geog <-> p.geog as dist
        from worldcities_ea
        where population >= 20000
        order by dist limit 1
    ) as w on true

    /**
    join lateral (
        select geog <-> p.geog as dist
        from mangroves
        order by dist limit 1
    ) as mg on true
    
    join lateral (
        select geog <-> p.geog as dist
        from corals
        order by dist limit 1
    ) as cr on true
    **/
);

drop table if exists mirrors_candidates;
create table mirrors_candidates as (
    select row_number() over() as fid, osm_id, country, place, name,
    d_wdpa_ea_selection, d_wdpa_any, d_coastline, d_cities_20k,
    --d_mangroves, d_corals,
    geom, st_buffer(geom::geography, 3000)::geometry as buffer_3km, st_buffer(geom::geography, 10000)::geometry as buffer_10km
    from tmp_mirrors
    where d_wdpa_ea_selection > 30 
    and d_coastline > 5
);
