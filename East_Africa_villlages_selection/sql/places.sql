drop table if exists osm_places_ea; 
create table osm_places_ea as (
    select row_number() over () as fid, a.id, a.lat, a.lon, a.country, a.place, a.name, a.population, a.geometry, a.geometry::geography as geog
    from osm_places_ea_full a, wdpa_ea_selection b
    where country in ('ET', 'MG', 'MZ', 'TZ', 'SZ')
    and place !='city'
    and st_intersects(a.geometry, b.buffer50)
);
create index osm_places_ea_geom_idx on osm_places_ea using gist(geometry);
create index osm_places_ea_geog_idx on osm_places_ea using gist(geog);
