drop table if exists wdpa_ea;
create table wdpa_ea as (
    select *, st_centroid(shape) as geom_point,
    shape::geography as geog, st_centroid(shape)::geography as geog_point
    from wdpa_poly_oct2021 where parent_iso3 in ('COM', 'SDN', 'SSD', 'MYT', 'COD', 'ZWE', 'MDG', 'RWA', 'ERI', 'TZA', 'ZMB', 'ZAF', 'KEN', 'SOM', 'DJI', 'UGA', 'MWI', 'SWZ', 'BDI', 'MOZ', 'LSO', 'ETH')
);

insert into wdpa_ea (
    objectid, wdpaid, wdpa_pid, pa_def, name, orig_name, desig, desig_eng, desig_type,
    iucn_cat, int_crit, marine, rep_m_area, rep_area, no_take, no_tk_area,
    status, status_yr, gov_type, own_type, mang_auth, mang_plan,
    verif, metadataid, sub_loc, parent_iso3, iso3, supp_info, cons_obj, geom_point, geog_point
) select *, shape::geography from wdpa_point_oct2021 where parent_iso3 in ('COM', 'SDN', 'SSD', 'MYT', 'COD', 'ZWE', 'MDG', 'RWA', 'ERI', 'TZA', 'ZMB', 'ZAF', 'KEN', 'SOM', 'DJI', 'UGA', 'MWI', 'SWZ', 'BDI', 'MOZ', 'LSO', 'ETH');

create index wdpa_ea_shape_idx on wdpa_ea using gist(shape);
create index wdpa_ea_geog_idx on wdpa_ea using gist(geog);
create index wdpa_ea_geom_point_idx on wdpa_ea using gist(geom_point);
create index wdpa_ea_geog_point_idx on wdpa_ea using gist(geog_point);

drop table if exists wdpa_ea_selection;
create table wdpa_ea_selection as (
    select *, st_buffer(coalesce(geog, geog_point), 20000)::geometry as buffer20, st_buffer(coalesce(geog, geog_point), 50000)::geometry as buffer50
    from wdpa_ea where wdpa_pid in ('555624671', '555697518', '555697527', '555698167', '555698171', '555705345_A', '20295', '342517', '342654', '365024', '555566988', '303695', '20273', '303702', '351704', '351706')
);
alter table wdpa_ea_selection add column "first_choice" integer;
update wdpa_ea_selection set first_choice = 1;
update wdpa_ea_selection set first_choice = 0 where wdpa_pid in ('303695', '20273', '303702', '351704', '351706');

create index wdpa_ea_selection_buffer_idx on wdpa_ea_selection using gist(buffer50);
