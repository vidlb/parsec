#!/bin/bash
PG_CONN="host=localhost port=5432 dbname=wdpa user=vidlb password='***'"

ogr2ogr -f "Postgresql" PG:$PG_CONN /media/data/vector/osm/coastlibeslines.shp -nln 'osm_coastlines' -overwrite -progress --config PG_USE_COPY YES
ogr2ogr -f "Postgresql" PG:$PG_CONN /media/data/vector/wdpa/WDPA_SepWDPA_Sep2020_Public.gdb -overwrite -progress --config PG_USE_COPY YES
ogr2ogr -f "Postgresql" PG:$PG_CONN /media/data/vector/jrc/ghs/GHS_STAT_UCDB2015MT_GLOBE_R2019A/GHS_STAT_UCDB2015MT_GLOBE_R2019A_V1_2.gpkg -nln 'ghs_ucdb_core' -overwrite -progress --config PG_USE_COPY YES
ogr2ogr -f "Postgresql" PG:$PG_CONN /media/data/vector/jrc/ghs/GHS_FUA_UCDB2015_GLOBE_R2019A_54009_1K_V1_0/GHS_FUA_UCDB2015_GLOBE_R2019A_54009_1K_V1_0.zip -nln 'ghs_ucdb_fua' -overwrite -progress --config PG_USE_COPY YES
