alter table wdpa_poly_sep2020 drop column if exists shape_length;
alter table wdpa_poly_sep2020 drop column if exists shape_area;
alter table wdpa_poly_sep2020 drop column if exists objectid;
alter table wdpa_point_sep2020 drop column if exists objectid;

alter table wdpa_point_sep2020 add column geog geography;
update wdpa_point_sep2020 set geog = shape::geography;

alter table wdpa_poly_sep2020 add column geog geography;
update wdpa_poly_sep2020 set geog = shape::geography;

drop table if exists wdpa;
create table wdpa as (
    select *, st_centroid(shape) as geom_point,
    shape::geography as geog, st_centroid(shape)::geography as geog_point
    from wdpa_poly_sep2020 where parent_iso3 in ('AUS','BRA','FRA','JPN','USA')
);

insert into wdpa (
    wdpaid, wdpa_pid, pa_def, name, orig_name, desig, desig_eng, desig_type,
    iucn_cat, int_crit, marine, rep_m_area, rep_area, no_take, no_tk_area,
    status, status_yr, gov_type, own_type, mang_auth, mang_plan,
    verif, metadataid, sub_loc, parent_iso3, iso3, geom_point, geog_point
) select *, shape::geography from wdpa_point_sep2020 where parent_iso3 in ('AUS','BRA','FRA','JPN','USA');

create index wdpa_shape_idx on wdpa using gist(shape);
create index wdpa_geog_idx on wdpa using gist(geog);
create index wdpa_geom_point_idx on wdpa using gist(geom_point);
create index wdpa_geog_point_idx on wdpa using gist(geog_point);

alter table ghs_ucdb_core add column geog geography;
update ghs_ucdb_core set geog = geom::geography;
create index ghs_ucdb_core_geog_idx on ghs_ucdb_core using brin(geog);

alter table ghs_ucdb_fua add column geog geography;
update ghs_ucdb_fua set geog = st_transform(geom, 4326)::geography;
create index ghs_ucdb_fua_geog_idx on ghs_ucdb_fua using brin(geog);
alter table ghs_ucdb_fua add column geom_4326 geometry(multipolygon);
update ghs_ucdb_fua set geom_4326 = st_transform(geom, 4326);
create index ghs_ucdb_fua_geom_4326_idx on ghs_ucdb_fua using gist(geom_4326);

alter table osm_coastlines add column geog geography;
update osm_coastlines set geog = wkb_geometry::geography;
create index osm_coastlines_geom_idx on osm_coastlines using gist(wkb_geometry);
create index osm_coastlines_geog_idx on osm_coastlines using gist(geog);

create table if not exists wdpa_d_coastlines as (
    select a.wdpa_pid, b.ogc_fid, b.distance/1000 as dist_km
    from wdpa a
    join lateral (
        select ogc_fid, coalesce(a.geog, a.geog_point) <-> geog as distance
        from osm_coastlines order by distance limit 1
    ) as b on true where a.marine='2'
);
\copy wdpa_d_coastlines to '/home/vidlb/Projets/pa/parsec/metrics/WDPA_d_coastlines.csv' csv header delimiter ',';

create table if not exists wdpa_d_fua as (
    select a.wdpa_pid, b.efua_id, b.efua_name, b.distance/1000 as dist_km
    from wdpa a
    join lateral (
        select efua_id, efua_name, coalesce(a.geog, a.geog_point) <-> geog as distance
        from ghs_ucdb_fua order by distance limit 1
    ) as b on true
);
\copy wdpa_d_fua to '/home/vidlb/Projets/pa/parsec/metrics/WDPA_d_FUA.csv' csv header delimiter ',';
