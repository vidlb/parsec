#!/usr/bin/env python
# coding: utf-8
import os
import re
import sys
from pathlib import Path

import sqlalchemy
import numpy as np
import pandas as pd
import geopandas as gpd
from rasterstats import zonal_stats, point_query

### ENV
WORKDIR=Path("/home/vidlb/Projets/pa/parsec/metrics")
COUNTRIES=('AUS','BRA','FRA','JPN','USA')
PG_STR="postgresql://vidlb@localhost:5432/parsec"

# Data path
DROOT=WORKDIR/'_data'
SRTM=DROOT/"SRTM15+V2.1.tif"
ELU=DROOT/"World_ELU_2015.tif"
GHM=DROOT/"gHM.tif"
GHS_POP=DROOT/"GHS_POP_E2015_GLOBE_R2019A_4326_9ss_V1_0.tif"
ACCESS=DROOT/"2015_accessibility_to_cities_v1.0.tif"

STATS={
#    ELU: ["World_ELU", None, "EPSG:4326"],
#    GHM: ["gHM", ['min', 'max', 'mean', 'median', 'std'], "ESRI:54009"],
#    GHS_POP: ["GHS_POP", ['min', 'max', 'mean', 'sum', 'std'], "EPSG:4326"],
#    SRTM: ["SRTM15", ['min', 'max', 'mean', 'std'], "EPSG:4326"],
    ACCESS: ["ACCESSIBILITY", ['min', 'max', 'mean', 'std'], "EPSG:4326"]
}


def pg2pd(conn_str, query, geomcol=""):
    conn = sqlalchemy.create_engine(conn_str)
    if geomcol:
        return gpd.GeoDataFrame.from_postgis(query, conn.raw_connection(), geomcol)

    cur = conn.raw_connection().cursor()
    cur.execute(query)
    return pd.DataFrame(cur.fetchall(), columns=[desc[0] for desc in cur.description])


def get_pa_data(iso3=""):
    query = "select *, coalesce(shape, geom_point) as geom from wdpa"
    if iso3:
        query += f" where parent_iso3='{iso3}'"
    pa = pg2pd(PG_STR, query, 'geom')
    pa.drop(columns=['shape', 'geom_point', 'geog', 'geog_point'], inplace=True)
    pa.columns = [c.upper() if  c != 'geom' else c for c in pa.columns]
    return pa


def process_zonal_stats(pa):
    polygons = pa[np.invert(pa.GIS_AREA.isna())].reset_index()
    points = pa[pa.GIS_AREA.isna()].reset_index()
    for key, values in STATS.items():
        print(f"Computing statistics for {key} ...")
        name, stats, srid = values
        if stats is None:
            res_polygons = polygons[['WDPA_PID']].join(pd.DataFrame(zonal_stats(polygons.to_crs(srid), key, categorical=True)))
        elif isinstance(stats, list):
            res_polygons = polygons[['WDPA_PID']].join(pd.DataFrame(zonal_stats(polygons.to_crs(srid), key, stats=stats)))
        res_polygons.to_csv(f"WDPA_polygons_stats_{name}.csv")
        res_points = points[['WDPA_PID']].join(pd.DataFrame(point_query(points.to_crs(srid), key)))
        res_points.to_csv(f"WDPA_points_stats_{name}.csv")

    print('Done.')

os.chdir(WORKDIR)

pa = get_pa_data()
process_zonal_stats(pa)
