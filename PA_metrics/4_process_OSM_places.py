#!/usr/bin/env python
# coding: utf-8
import os
import re
import sys
import requests
import time
from pathlib import Path

import simplejson
import numpy as np
import pandas as pd
import geopandas as gpd

WORKDIR=Path("/home/vidlb/Projets/pa/parsec/metrics")
PG_STR="postgresql://vidlb@localhost:5432/parsec"

COUNTRIES=('AUS','BRA','FRA','JPN','USA')
PLACES=['city','town','village','hamlet']
Q_OSM_PLACES="""
create table if not exists wdpa_c_osm_places as (
    select a.wdpa_pid, b.place, count(b.id) as count
    from wdpa a, osm_places b
    where substr(a.parent_iso3,1,2) = b.country
    and st_dwithin(b.geog, coalesce(a.geog, a.geog_point), 20000)
    group by wdpa_pid, place
);
"""


def get_osm_places(code_iso: str,
                   places: list = PLACES,
                   timeout: int = 900):
    """Download OSM places with the overpass API"""

    if len(code_iso) == 3:
        code_iso = code_iso[:2]
    places_regex = "|".join([f'^{p}$' for p in places])

    overpass_url = "http://overpass.openstreetmap.fr/api/interpreter"
    overpass_query = f"""
        [out:json][timeout:{timeout}];
        area["ISO3166-1"="{code_iso}"][admin_level=2];
        node["place"~"{places_regex}"][!"end_date"](area);
        out; """

    response = requests.get(overpass_url, params={'data': overpass_query})

    if response.status_code == 400:
        print('Bad request !')
        return None
    elif response.status_code == 429:
        print('Too many requests !')
        return None
    elif response.status_code != 200:
        print(f"HTTP:{response.status_code}")

    try:
        res = pd.DataFrame(response.json()['elements'])
    except simplejson.errors.JSONDecodeError:
        print(response.content)
        return
    data = gpd.GeoDataFrame(res, geometry=gpd.points_from_xy(res.lon, res.lat))
    def tags_extraction(tags):
        population = re.sub("[^0-9]", "", tags.get('population', '0'))
        try:
            pop = int(population)
        except ValueError:
            pop = 0
        return pd.Series(index=['country', 'place', 'name', 'population'],
                         data=(code_iso, tags.get('place'), tags.get('name'), pop))

    return data.join(data['tags'].apply(tags_extraction))


def osm_process(conn_str=PG_STR):
    os.chdir(WORKDIR)
    osm_data = []
    for country in COUNTRIES:
        if not os.path.exists(country):
            os.mkdir(country)
        outfile = f"{country}/OSM_places_{country}.json"
        if not Path(outfile).exists():
            places = get_osm_places(country)
            if places is not None:
                places.to_file(outfile, driver="GeoJSON")
        else:
            places = gpd.read_file(outfile)
        osm_data.append(places)
        time.sleep(10)


    #conn = sqlalchemy.create_engine(conn_str)
    #osm_places=pd.concat(osm_data)
    #osm_places[osm_places.columns[1:]].to_postgis('osm_places', conn)
    #conn.execute("create index osm_places_geom_idx on osm_places using gist(geometry);")
    #conn.execute(Q_OSM_PLACES)

if __name__ == '__main__':
    osm_process()
